﻿<?php
$title="Новости";
$color="#aaddff";

include('header.php');
include('connect.php');

$query1 = "SELECT * FROM news order by id_news";
$result1 = $mysqli->query($query1);
$result2 = $mysqli->query($query1);
?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php while ($row1 = $result1->fetch_assoc())
            { ?>
                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $row1["id_news"]?>" class="<?php if ($row1["id_news"]=="0") echo "active"?>"></li>
            <?php }?>
        </ol>

        <div class="carousel-inner">

            <?php while ($row2 = $result2->fetch_assoc())
            { ?>
                <div class="<?php if ($row2["id_news"]=="0") echo "active item"; else echo "item"; ?>">
                    <img src="<?php echo $row2["картинка"] ?>" >
                    <div class="carousel-caption">
                        <h3><?php echo $row2["текст"] ?></h3>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

<?php $mysqli->close();
include("footer.php"); ?>